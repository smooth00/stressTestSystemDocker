# Use JDK-8 on alpine.
From openjdk:8-alpine
MAINTAINER smooth00 <smooth.blog.csdn.net>

ENV JMETER_VERSION 5.1.1

# Install Pre-requisite Packages like wget
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories && \
    apk update && apk add wget unzip vim && apk add -U tzdata && \
    ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone

# Installing jmeter
RUN mkdir /jmeter \
	&& cd /jmeter/ \
	&& wget https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-$JMETER_VERSION.tgz \
	&& tar -xzf apache-jmeter-$JMETER_VERSION.tgz \
	&& rm apache-jmeter-$JMETER_VERSION.tgz \
	&& mkdir -p /jmeter/apache-jmeter-$JMETER_VERSION/bin/stressTestCases \
	&& wget -P /jmeter/apache-jmeter-$JMETER_VERSION/lib/ext https://repo.maven.apache.org/maven2/kg/apc/jmeter-plugins-extras-libs/1.4.0/jmeter-plugins-extras-libs-1.4.0.jar \
	&& wget -P /jmeter/apache-jmeter-$JMETER_VERSION/lib/ext https://repo.maven.apache.org/maven2/kg/apc/jmeter-plugins-standard/1.4.0/jmeter-plugins-standard-1.4.0.jar

# COPY ApacheJMeter_core.jar /jmeter/apache-jmeter-$JMETER_VERSION/lib/ext/ApacheJMeter_core.jar

# Settingt Jmeter Home
ENV JMETER_HOME /jmeter/apache-jmeter-$JMETER_VERSION

# Finally Adding Jmeter to the Path
ENV PATH $JMETER_HOME/bin:$PATH

# Volume directory to be mapped (for test files)
VOLUME $JMETER_HOME/bin/stressTestCases

# Ports to be exposed from the container for JMeter Slaves/Server
EXPOSE 1099 50000
ENV SSL_DISABLED true

# Application to run on starting the container
WORKDIR $JMETER_HOME/bin/stressTestCases
ENTRYPOINT ../jmeter-server \
                        -Dserver.rmi.localport=50000 \
                        -Dserver_port=1099 \
                        -Jserver.rmi.ssl.disable=${SSL_DISABLED} \
                        -Djava.rmi.server.hostname=${HOST_IP}